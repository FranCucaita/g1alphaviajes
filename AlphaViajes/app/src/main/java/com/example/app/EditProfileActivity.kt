package com.example.app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.firestore.FirebaseFirestore
import org.w3c.dom.Text

class EditProfileActivity : AppCompatActivity() {
    private val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)

        val bundle: Bundle? = intent.extras
        val email: String? =bundle?.getString("email")
        setup(email ?: "")

        val button = findViewById<Button>(R.id.btnSave)

        val btnBack: ImageButton = findViewById(R.id.btnBackEdit)
        btnBack.setOnClickListener {
            val intent: Intent = Intent(this, ProfileActivity::class.java).apply {
                putExtra("email", email)
            }
            startActivity(intent)
        }

        button.setOnClickListener {

            saveData(email ?: "")

            val builder = AlertDialog.Builder(this)
            //set title for alert dialog
            builder.setTitle("Información Actualizada")
            //set message for alert dialog
            builder.setMessage("Su información se ha actualizado exitosamente")

            //performing positive action
            builder.setPositiveButton("Aceptar"){dialogInterface, which ->
                val intent: Intent = Intent(this, ProfileActivity::class.java).apply {
                    putExtra("email", email)
                }
                startActivity(intent)
            }

            // Create the AlertDialog
            val alertDialog: AlertDialog = builder.create()
            // Set other dialog properties
            alertDialog.setCancelable(false)
            alertDialog.show()
        }


    }

    private fun setup(email: String){
        val titleName: TextView = findViewById(R.id.titleName)
        val name: TextInputEditText = findViewById(R.id.name)
        val Email: TextInputEditText = findViewById(R.id.email)
        val phone: TextInputEditText = findViewById(R.id.phone)
        val location: TextInputEditText = findViewById(R.id.location)
        db.collection("users").document(email).get().addOnSuccessListener {
            name.setText(it.get("name") as String?)
            titleName.setText(it.get("name") as String?)
            phone.setText(it.get("phone") as String?)
            location.setText(it.get("location") as String?)
        }
        Email.setText(email)
    }

    private fun saveData(email: String){
        val name: TextInputEditText = findViewById(R.id.name)
        val phone: TextInputEditText = findViewById(R.id.phone)
        val location: TextInputEditText = findViewById(R.id.location)
        db.collection("users").document(email).set(
            hashMapOf("name" to name.text.toString(),
                "phone" to phone.text.toString(),
                "location" to location.text.toString())
        )
    }
}