package com.example.app

import android.content.Intent
import android.os.Bundle
import android.widget.ImageButton
import androidx.appcompat.app.AppCompatActivity

class HomeActivity : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {

        //Thread.sleep(2000)
        setTheme(R.style.ThemeApp)

        val bundle: Bundle? = intent.extras
        val email: String? =bundle?.getString("email")

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val btnToLogin: ImageButton = findViewById(R.id.btnLogIn)
        btnToLogin.setOnClickListener {
            val intent: Intent = Intent(this, LogInActivity::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.zoom_back_in, R.anim.zoom_back_out)
        }

        val btnToProfile: ImageButton = findViewById(R.id.btnProfile)
        btnToProfile.setOnClickListener {
            val intent: Intent = Intent(this, ProfileActivity::class.java).apply {
                putExtra("email", email)
            }
            startActivity(intent)
            overridePendingTransition(R.anim.zoom_back_in, R.anim.zoom_back_out)
        }

        val onClickHotels: ImageButton = findViewById(R.id.btnHotels)
        onClickHotels.setOnClickListener {
            val intent = Intent(this, HotelsActivity::class.java).apply {
                putExtra("email", email)
            }
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.left_out)
        }
        val onClickTickets: ImageButton = findViewById(R.id.btnTickets)
        onClickTickets.setOnClickListener {
            val intent = Intent(this, TicketsActivity::class.java).apply {
                putExtra("email", email)
            }
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.right_out)
        }

        val btnOfertas: ImageButton = findViewById(R.id.btnOfertas)
        btnOfertas.setOnClickListener {
            val intent = Intent(this, UnderConstructionActivity::class.java).apply {
                putExtra("email", email)
            }
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.left_out)
        }

        val btnOtros: ImageButton = findViewById(R.id.btnOtros)
        btnOtros.setOnClickListener {
            val intent = Intent(this, UnderConstructionActivity::class.java).apply {
                putExtra("email", email)
            }
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.right_out)
        }
    }
}