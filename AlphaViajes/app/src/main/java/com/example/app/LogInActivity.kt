package com.example.app

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.ImageButton
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth

class LogInActivity : AppCompatActivity(){
    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.fade_in, R.anim.zoom_back_out)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val Register: Button = findViewById(R.id.btnRegister)
        Register.setOnClickListener {
            val intent: Intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.zoom_back_out)
        }
        val btnBackToHome: ImageButton = findViewById(R.id.btnBackToHome)
        btnBackToHome.setOnClickListener {
            val intent: Intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.zoom_back_out)
        }

        setup()
    }

    private fun setup(){
        val et_email: TextInputEditText = findViewById(R.id.et_email)
        val et_password: TextInputEditText = findViewById(R.id.et_password)


        val login: Button = findViewById(R.id.btnLogin)
        login.setOnClickListener{
            if (!et_email.text.isNullOrEmpty() && !et_password.text.isNullOrEmpty()){
                FirebaseAuth.getInstance().signInWithEmailAndPassword(et_email.text.toString(),
                    et_password.text.toString()).addOnCompleteListener{
                    if (it.isSuccessful){
                        showHome(it.result?.user?.email.toString() ?:"default email")
                        overridePendingTransition(R.anim.fade_in, R.anim.zoom_back_out)
                    }else {
                        showAlert()
                    }
                }
            } else {
                showAlert()
            }
        }
    }

    private fun showAlert(){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Error")
        builder.setMessage("Se ha producido un error autenticando al usuario")
        builder.setPositiveButton("Aceptar", null)
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    private fun showHome(email: String){
        val intent: Intent = Intent(this, ProfileActivity::class.java).apply {
            putExtra("email", email)
        }
        startActivity(intent)
    }
}