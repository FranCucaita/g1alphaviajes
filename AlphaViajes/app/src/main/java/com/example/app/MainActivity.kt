package com.example.app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.google.android.material.shape.TriangleEdgeTreatment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        //Thread.sleep(2000)

        setTheme(R.style.ThemeApp)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
    }
}