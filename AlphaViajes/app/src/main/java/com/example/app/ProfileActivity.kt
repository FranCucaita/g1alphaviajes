package com.example.app

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.button.MaterialButton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class ProfileActivity: AppCompatActivity() {
    val db = FirebaseFirestore.getInstance()

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.fade_out, R.anim.zoom_back_out)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        val bundle: Bundle? = intent.extras
        val email: String? =bundle?.getString("email")
        setup(email ?: "")

        val onClickHome: Button = findViewById(R.id.btnReservations)
        onClickHome.setOnClickListener {
            val intent: Intent = Intent(this, ReservationActivity::class.java).apply {
                putExtra("email", email)
            }
            startActivity(intent)
        }

        val btnEditarPerfil: ImageButton = findViewById(R.id.btnEditProfile)
        btnEditarPerfil.setOnClickListener {
            val intent: Intent = Intent(this, EditProfileActivity::class.java).apply {
                putExtra("email", email)
            }
            startActivity(intent)
        }

        val btnBackToHome: ImageButton = findViewById(R.id.btnBackToHome)
        btnBackToHome.setOnClickListener {
            val intent: Intent = Intent(this, HomeActivity::class.java).apply {
                putExtra("email", email)
            }
            startActivity(intent)
        }

        val logoutButton: MaterialButton = findViewById(R.id.logout)
        logoutButton.setOnClickListener{
            FirebaseAuth.getInstance().signOut()
            val intent: Intent = Intent(this, LogInActivity::class.java)
            startActivity(intent)
        }
    }

    private fun setup(email: String){
        val titleName: TextView =findViewById(R.id.titleName)
        val name: TextView =findViewById(R.id.name)
        val userEmail: TextView =findViewById(R.id.Email)
        val phone: TextView =findViewById(R.id.phone)
        val location: TextView =findViewById(R.id.location)
        db.collection("users").document(email).get().addOnSuccessListener {
            name.setText(it.get("name") as String?)
            titleName.setText(it.get("name") as String?)
            phone.setText(it.get("phone") as String?)
            location.setText(it.get("location") as String?)
        }
        userEmail.text = email
    }
}