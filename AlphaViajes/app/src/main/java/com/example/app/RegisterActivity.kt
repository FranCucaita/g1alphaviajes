package com.example.app

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.ImageButton
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase

class RegisterActivity : AppCompatActivity() {
    private val db = FirebaseFirestore.getInstance()

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.fade_in, R.anim.zoom_back_out)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        val btnBackLogin: ImageButton = findViewById(R.id.btnBackLogin)
        btnBackLogin.setOnClickListener {
            val intent: Intent = Intent(this, LogInActivity::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.zoom_back_out)
        }
        setup()
    }

    private fun setup(){
        val signUp: Button = findViewById(R.id.btnRegister)
        val email: TextInputEditText = findViewById(R.id.email)
        val password: TextInputEditText = findViewById(R.id.password)
        val verifyPassword: TextInputEditText = findViewById(R.id.verifyPassword)
        signUp.setOnClickListener{
            if (!verifyPassword.text.toString().equals(password.text.toString())){
                showAlertPassword()
            } else {
                if (!email.text.isNullOrEmpty() && !password.text.isNullOrEmpty()){
                    FirebaseAuth.getInstance().createUserWithEmailAndPassword(email.text.toString(),
                        password.text.toString()).addOnCompleteListener{
                        if (it.isSuccessful){
                            showHome(it.result?.user?.email.toString() ?:"default email")
                            overridePendingTransition(R.anim.fade_in, R.anim.zoom_back_out)
                        }else {
                            showAlert()
                        }
                    }
                }
            }

        }
    }

    private fun showAlert(){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Error")
        builder.setMessage("Se ha producido un error autenticando al usuario")
        builder.setPositiveButton("Aceptar", null)
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    private fun showAlertPassword(){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Error")
        builder.setMessage("Las contraseñas no coinciden, verifique nuevamente")
        builder.setPositiveButton("Aceptar", null)
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    private fun showHome(email: String){
        val name: TextInputEditText = findViewById(R.id.name)
        val phone: TextInputEditText = findViewById(R.id.phone)
        val location: TextInputEditText = findViewById(R.id.location)
        db.collection("users").document(email).set(
            hashMapOf("name" to name.text.toString(),
            "phone" to phone.text.toString(),
            "location" to location.text.toString())
        )


        val builder = AlertDialog.Builder(this)
        //set title for alert dialog
        builder.setTitle("Registro exitoso")
        //set message for alert dialog
        builder.setMessage("Su información se ha registrado exitosamente")

        //performing positive action
        builder.setPositiveButton("Aceptar"){dialogInterface, which ->
            val intent: Intent = Intent(this, ProfileActivity::class.java).apply {
                putExtra("email", email)
            }
            startActivity(intent)
        }
        // Create the AlertDialog
        val alertDialog: AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        alertDialog.show()


    }
}