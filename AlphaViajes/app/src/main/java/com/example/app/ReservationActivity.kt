package com.example.app

import android.content.Intent
import android.os.Bundle
import android.widget.ImageButton
import androidx.appcompat.app.AppCompatActivity

class ReservationActivity : AppCompatActivity(){
    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.fade_in, R.anim.zoom_back_out)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reservations)

        val bundle: Bundle? = intent.extras
        val email: String? =bundle?.getString("email")

        val btnBackProfile: ImageButton = findViewById(R.id.btnBackProfile)
        btnBackProfile.setOnClickListener {
            val intent: Intent = Intent(this, ProfileActivity::class.java).apply {
                putExtra("email", email)
            }
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.zoom_back_out)
        }

    }
}