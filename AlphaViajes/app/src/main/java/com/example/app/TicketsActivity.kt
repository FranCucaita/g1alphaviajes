package com.example.app

import android.content.Intent
import android.os.Bundle
import android.widget.ImageButton
import androidx.appcompat.app.AppCompatActivity


class TicketsActivity : AppCompatActivity(){
    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.fade_in, R.anim.left_out)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tickets)

        val bundle: Bundle? = intent.extras
        val email: String? =bundle?.getString("email")

        val btnBackToHome: ImageButton = findViewById(R.id.btnBackToHome)
        btnBackToHome.setOnClickListener {
            val intent: Intent = Intent(this, HomeActivity::class.java).apply {
                putExtra("email", email)
            }
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.left_out)
        }
    }
}