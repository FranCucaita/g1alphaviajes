package com.example.app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageButton

class UnderConstructionActivity : AppCompatActivity() {
    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.fade_in, R.anim.zoom_back_out)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_under_construction)

        val bundle: Bundle? = intent.extras
        val email: String? =bundle?.getString("email")

        val btnBackToHome: ImageButton = findViewById(R.id.btnBackToHome)
        btnBackToHome.setOnClickListener {
            val intent = Intent(this, HomeActivity::class.java).apply {
                putExtra("email", email)
            }
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.zoom_back_out)
        }

    }
}